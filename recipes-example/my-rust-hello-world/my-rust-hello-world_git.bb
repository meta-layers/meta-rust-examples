inherit cargo

SRC_URI = "git://gitlab.com/exempli-gratia/my-rust-hello-world.git;protocol=https;branch=main"
SRCREV="33c9b6c3b27d98ac6c60ebbfccf2d34e01b417d7"
LIC_FILES_CHKSUM="file://COPYRIGHT;md5=e6b2207ac3740d2d01141c49208c2147 \
"


SUMMARY = "Hello World by Cargo for Rust"
HOMEPAGE = "https://github.com/meta-rust/rust-hello-world"
LICENSE = "MIT"

S = "${WORKDIR}/git"

BBCLASSEXTEND = "native"

# ERROR: my-rust-hello-world-git-r0 do_compile: ExecutionError('/workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/temp/run.do_compile.125801', 101, None, None)
# ERROR: Logfile of failure stored in: /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/temp/log.do_compile.125801
# Log data follows:
# | DEBUG: Executing shell function do_compile
# | NOTE: Using rust targets from /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/rust-targets/
# | NOTE: cargo = /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/recipe-sysroot-native/usr/bin/cargo
# | NOTE: cargo build -v --frozen --target armv7-resy-linux-gnueabihf --release --manifest-path=/workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/git//Cargo.toml
# | error: the lock file /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/git/Cargo.lock needs to be updated but --frozen was passed to prevent this
# | If you want to try to generate the lock file without accessing the network, remove the --frozen flag and use --offline instead.
# | WARNING: /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/temp/run.do_compile.125801:187 exit 101 from '"cargo" build -v --frozen --target armv7-resy-linux-gnueabihf --release --manifest-path=/workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/git//Cargo.toml "$@"'
# | WARNING: Backtrace (BB generated script):
# |       #1: oe_cargo_build, /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/temp/run.do_compile.125801, line 187
# |       #2: cargo_do_compile, /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/temp/run.do_compile.125801, line 159
# |       #3: do_compile, /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/temp/run.do_compile.125801, line 154
# |       #4: main, /workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/my-rust-hello-world/git/temp/run.do_compile.125801, line 200
# ERROR: Task (/workdir/sources/meta-rust-examples/recipes-example/my-rust-hello-world/my-rust-hello-world_git.bb:do_compile) failed with exit code '1'
# NOTE: Tasks Summary: Attempted 957 tasks of which 945 didn't need to be rerun and 1 failed.
# NOTE: The errors for this build are stored in /workdir/build/multi-v7-ml-debug-training-master/tmp/log/error-report/error_report_20240710191425.txt
# You can send the errors to a reports server by running:
#   send-error-report /workdir/build/multi-v7-ml-debug-training-master/tmp/log/error-report/error_report_20240710191425.txt [-s server]
#   NOTE: The contents of these logs will be posted in public if you use the above command with the default server. Please ensure you remove any identifying or proprietary information when prompted before sending.
#   NOTE: Writing buildhistory
#   NOTE: Writing buildhistory took: 2 seconds
#
#   Summary: 1 task failed:
#     /workdir/sources/meta-rust-examples/recipes-example/my-rust-hello-world/my-rust-hello-world_git.bb:do_compile
#     Summary: There was 1 ERROR message, returning a non-zero exit code.
#

# Fix:
# hacked poky which allows this:
CARGO_NO_FROZEN = "1"
