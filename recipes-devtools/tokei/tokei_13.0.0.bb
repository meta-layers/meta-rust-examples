# Auto-Generated by cargo-bitbake 0.3.16-alpha.0
#
inherit cargo cargo-update-recipe-crates

# If this is git based prefer versioned ones if they exist
# DEFAULT_PREFERENCE = "-1"

# how to get tokei could be as easy as but default to a git checkout:
# SRC_URI += "crate://crates.io/tokei/13.0.0-alpha.1"
SRC_URI += "git://github.com/XAMPPRocky/tokei;protocol=https;nobranch=1"
SRCREV = "6392516c47d4573d16886b9fe5f79592b1c70d49"
S = "${WORKDIR}/git"
CARGO_SRC_DIR = ""
PV:append = ".AUTOINC+6392516c47"

require ${BPN}-crates.inc

LIC_FILES_CHKSUM = " \
    file://LICENCE-APACHE;md5=667a9ab3b81e6041e2414493e3a551e4 \
    file://LICENCE-MIT;md5=68cf3aebc0400ea3f11dafb5f45992fd \
"

SUMMARY = "Count your code, quickly."
HOMEPAGE = "https://tokei.rs"
LICENSE = "MIT | Apache-2.0"

# includes this file if it exists but does not fail
# this is useful for anything you may want to override from
# what cargo-bitbake generates.
include tokei-${PV}.inc
include tokei.inc

SRC_URI += "file://0001-remove-panic-abort-from-Cargo.toml.patch"

BBCLASSEXTEND = "native nativesdk"
