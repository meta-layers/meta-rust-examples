*) enter cooker mode
*) source meta-ide support environment script

cd /workdir/build/multi-v7-ml-debug-training/tmp/deploy/images/multi-v7-ml
source environment-setup-armv7at2hf-neon-resy-linux-gnueabi

*) find source dir

bitbake starship -e | grep ^S=

*) cd into source dir

cd /workdir/build/multi-v7-ml-debug-training/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/starship/1.18.2/git

*) run the tool

/workdir/sources/meta-rust-examples/recipes-devtools/find-duplicate-crates/find-duplicate-crates.py

pokyuser@hp-zbook-15-g6-18:/workdir/build/multi-v7-ml-debug-training/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/starship/1.18.2/git$ /workdir/sources/meta-rust-examples/recipes-devtools/find-duplicate-crates/find-duplicate-crates.py
    Updating crates.io index
  Downloaded adler v1.0.2
  Downloaded allocator-api2 v0.2.16
  Downloaded android_system_properties v0.1.5
  Downloaded android-tzdata v0.1.1
  Downloaded anstyle v1.0.4
  ...
  Downloaded windows v0.54.0
  Downloaded windows v0.51.1
  Downloaded 381 crates (71.3 MB) in 16.16s (largest was `windows` at 12.3 MB)

async-io has multiple version: ['1.13.0', '2.2.2']
async-lock has multiple version: ['2.8.0', '3.2.0']
bitflags has multiple version: ['1.3.2', '2.4.1']
block-buffer has multiple version: ['0.9.0', '0.10.4']
digest has multiple version: ['0.9.0', '0.10.7']
dirs has multiple version: ['4.0.0', '5.0.1']
dirs-sys has multiple version: ['0.3.7', '0.4.1']
errno has multiple version: ['0.2.8', '0.3.8']
event-listener has multiple version: ['2.5.3', '3.1.0', '4.0.1']
fastrand has multiple version: ['1.9.0', '2.0.1']
futures-lite has multiple version: ['1.13.0', '2.1.0']
itertools has multiple version: ['0.11.0', '0.12.0']
linux-raw-sys has multiple version: ['0.3.8', '0.4.12']
memoffset has multiple version: ['0.7.1', '0.9.0']
nix has multiple version: ['0.26.4', '0.28.0']
nom has multiple version: ['5.1.3', '7.1.3']
polling has multiple version: ['2.8.0', '3.3.1']
quick-xml has multiple version: ['0.30.0', '0.31.0']
rustix has multiple version: ['0.37.27', '0.38.31']
semver has multiple version: ['0.11.0', '1.0.22']
sha2 has multiple version: ['0.9.9', '0.10.8']
signal-hook has multiple version: ['0.1.17', '0.3.17']
syn has multiple version: ['1.0.109', '2.0.53']
toml has multiple version: ['0.5.11', '0.8.12']
toml_edit has multiple version: ['0.19.15', '0.22.9']
windows has multiple version: ['0.51.1', '0.54.0']
windows-core has multiple version: ['0.51.1', '0.54.0']
windows-sys has multiple version: ['0.48.0', '0.52.0']
windows-targets has multiple version: ['0.48.5', '0.52.3']
windows_aarch64_gnullvm has multiple version: ['0.48.5', '0.52.3']
windows_aarch64_msvc has multiple version: ['0.48.5', '0.52.3']
windows_i686_gnu has multiple version: ['0.48.5', '0.52.3']
windows_i686_msvc has multiple version: ['0.48.5', '0.52.3']
windows_x86_64_gnu has multiple version: ['0.48.5', '0.52.3']
windows_x86_64_gnullvm has multiple version: ['0.48.5', '0.52.3']
windows_x86_64_msvc has multiple version: ['0.48.5', '0.52.3']
winnow has multiple version: ['0.5.40', '0.6.5']
