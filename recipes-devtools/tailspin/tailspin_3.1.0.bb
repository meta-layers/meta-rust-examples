# Auto-Generated by cargo-bitbake 0.3.16-alpha.0
#
inherit cargo cargo-update-recipe-crates

# If this is git based prefer versioned ones if they exist
# DEFAULT_PREFERENCE = "-1"

# how to get tailspin could be as easy as but default to a git checkout:
# SRC_URI += "crate://crates.io/tailspin/3.1.0"
SRC_URI += "git://github.com/bensadeh/tailspin.git;protocol=https;nobranch=1;branch=main"
SRCREV = "bd75c19458da9bc6c35d1ac9919c19d33d6b1c57"
S = "${WORKDIR}/git"
CARGO_SRC_DIR = ""
PV:append = ".AUTOINC+bd75c19458"

require ${BPN}-crates.inc

LIC_FILES_CHKSUM = " \
    file://LICENCE;md5=3668133521a610c9faab9bcfed2501e4 \
"

SUMMARY = "A log file highlighter"
HOMEPAGE = "https://github.com/bensadeh/tailspin"
LICENSE = "MIT"

# includes this file if it exists but does not fail
# this is useful for anything you may want to override from
# what cargo-bitbake generates.
include tailspin-${PV}.inc
include tailspin.inc

BBCLASSEXTEND = "native nativesdk"
