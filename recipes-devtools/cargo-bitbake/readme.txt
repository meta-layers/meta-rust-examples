######

# inherit cargo-update-recipe-crates
bitbake -c update_crates cargo-bitbake

bitbake cargo-bitbake-native

bitbake cargo-bitbake-native -c addto_recipe_sysroot
oe-run-native cargo-bitbake-native cargo-bitbake --help

oe-run-native cargo-bitbake-native cargo-bitbake bitbake

######

## Introduction

This provides the Rust compiler, tools for building packages (cargo), and 
a few example projects.

## Building a rust package

When building a rust package in bitbake, it's usually easiest to build with
cargo using cargo.bbclass.  If the package already has a Cargo.toml file (most
rust packages do), then it's especially easy.  Otherwise you should probably
get the code building in cargo first. 

Once your package builds in cargo, you can use
[cargo-bitbake](https://github.com/cardoe/cargo-bitbake) to generate a bitbake
recipe for it.  This allows bitbake to fetch all the necessary dependent
crates, as well as a pegged version of the crates.io index, to ensure maximum
reproducibility. Once the Rust SDK support is added to oe-core, cargo-bitbake
may also be added to the SDK.

NOTE: You will have to edit the generated recipe based on the comments
contained within it

## Pitfalls

 - TARGET_SYS _must_ be different from BUILD_SYS. This is due to the way Rust
 configuration options are tracked for different targets. This is the reason 
 we use the Yocto triples instead of the native Rust triples. See rust-lang/cargo#3349.

## Dependencies

On the host:
 - Any `-sys` packages your project might need must have RDEPENDs for
 the native library.

On the target:
 - Any `-sys` packages your project might need must have RDEPENDs for
 the native library.

==============================
bitbake cargo-native

bitbake cargo-native -c addto_recipe_sysroot

#oe-run-native cargo-native cargo install cargo-bitbake

------

#fails because it misses openssl, so let's add it

#bitbake openssl-native -c addto_recipe_sysroot

#bitbake pkgconfig-native -c addto_recipe_sysroot

#oe-run-native cargo-native cargo install cargo-bitbake

----

try something else:

oe-run-native cargo-native cargo install maturin

Running bitbake -e cargo-native
    Updating crates.io index
  Downloaded maturin v0.14.6
  Downloaded 1 crate (163.5 KB) in 1.05s
  Installing maturin v0.14.6
  Downloaded console v0.15.2
  Downloaded encoding-index-singlebyte v1.20141219.5
  Downloaded nu-ansi-term v0.46.0
  Downloaded mime v0.3.16
  Downloaded clap_derive v3.2.18
  Downloaded rayon-core v1.10.1
  Downloaded scopeguard v1.1.0
  Downloaded thiserror v1.0.37
  Downloaded generic-array v0.14.6
  Downloaded rand_chacha v0.3.1
  Downloaded smallvec v1.10.0
  Downloaded thiserror-impl v1.0.37
  Downloaded untrusted v0.7.1
  Downloaded bincode v1.3.3
  Downloaded block-buffer v0.10.3
  Downloaded base64 v0.13.1
  Downloaded ahash v0.7.6
  Downloaded sharded-slab v0.1.4
  Downloaded static_assertions v1.1.0
  Downloaded cargo_metadata v0.15.2
  Downloaded bzip2 v0.4.3
  Downloaded spin v0.5.2
  Downloaded adler v1.0.2
  Downloaded rustc_version v0.4.0
  Downloaded time v0.3.17
  Downloaded multipart v0.18.0
  Downloaded time-core v0.1.0
  Downloaded tracing v0.1.37
  Downloaded tracing-serde v0.1.3
  Downloaded xattr v0.2.3
  Downloaded tracing-log v0.1.3
  Downloaded zip v0.6.3
  Downloaded uuid v1.2.2
  Downloaded dirs v4.0.0
  Downloaded scroll_derive v0.11.0
  Downloaded plain v0.2.3
  Downloaded ureq v2.5.0
  Downloaded sha2 v0.10.6
  Downloaded dirs-sys v0.3.7
  Downloaded tracing-attributes v0.1.23
  Downloaded crossbeam-epoch v0.9.13
  Downloaded chunked_transfer v1.4.0
  Downloaded clap_complete_fig v4.0.2
  Downloaded io-lifetimes v1.0.3
  Downloaded memoffset v0.7.1
  Downloaded getrandom v0.2.8
  Downloaded indicatif v0.17.2
  Downloaded encoding-index-japanese v1.20141219.5
  Downloaded dunce v1.0.3
  Downloaded fs-err v2.9.0
  Downloaded paste v1.0.10
  Downloaded number_prefix v0.4.0
  Downloaded const-random-macro v0.1.15
  Downloaded clap_derive v4.0.21
  Downloaded askama_derive v0.11.2
  Downloaded byteorder v1.4.3
  Downloaded clap v4.0.29
  Downloaded portable-atomic v0.3.16
  Downloaded which v4.3.0
  Downloaded proc-macro-hack v0.5.19
  Downloaded webpki-roots v0.22.6
  Downloaded rfc2047-decoder v0.2.0
  Downloaded target-lexicon v0.12.5
  Downloaded socks v0.3.4
  Downloaded dialoguer v0.10.2
  Downloaded terminal_size v0.1.17
  Downloaded is-terminal v0.4.1
  Downloaded heck v0.4.0
  Downloaded askama v0.11.1
  Downloaded digest v0.10.6
  Downloaded parking_lot v0.12.1
  Downloaded overload v0.1.1
  Downloaded crossbeam-deque v0.8.2
  Downloaded cpufeatures v0.2.5
  Downloaded clap_lex v0.3.0
  Downloaded const-random v0.1.15
  Downloaded askama_escape v0.10.3
  Downloaded pin-project-lite v0.2.9
  Downloaded camino v1.1.1
  Downloaded path-slash v0.2.1
  Downloaded ppv-lite86 v0.2.17
  Downloaded encoding v0.2.33
  Downloaded uniffi_meta v0.21.0
  Downloaded rand v0.8.5
  Downloaded parking_lot_core v0.9.5
  Downloaded matchers v0.1.0
  Downloaded tiny-keccak v2.0.2
  Downloaded time-macros v0.2.6
  Downloaded cab v0.4.1
  Downloaded crossbeam-channel v0.5.6
  Downloaded unicase v2.6.0
  Downloaded askama_shared v0.12.2
  Downloaded ahash v0.3.8
  Downloaded nom v7.1.1
  Downloaded miniz_oxide v0.6.2
  Downloaded cbindgen v0.24.3
  Downloaded weedle2 v4.0.0
  Downloaded twox-hash v1.6.3
  Downloaded versions v4.1.0
  Downloaded unicode-linebreak v0.1.4
  Downloaded lzxd v0.1.4
  Downloaded encoding-index-korean v1.20141219.5
  Downloaded xwin v0.2.10
  Downloaded quoted_printable v0.4.6
  Downloaded terminal_size v0.2.3
  Downloaded rustix v0.36.5
  Downloaded scroll v0.11.0
  Downloaded platform-info v1.0.1
  Downloaded minimal-lexical v0.2.1
  Downloaded pyproject-toml v0.3.1
  Downloaded normpath v1.0.0
  Downloaded encoding-index-tradchinese v1.20141219.5
  Downloaded toml_edit v0.15.0
  Downloaded cargo-zigbuild v0.14.2
  Downloaded encoding_index_tests v0.1.4
  Downloaded toml_datetime v0.5.0
  Downloaded rustls v0.20.7
  Downloaded encoding-index-simpchinese v1.20141219.5
  Downloaded chumsky v0.8.0
  Downloaded uniffi_bindgen v0.21.0
  Downloaded cli-table v0.4.7
  Downloaded charset v0.1.3
  Downloaded configparser v3.0.2
  Downloaded clap_complete v4.0.6
  Downloaded msi v0.5.0
  Downloaded mime_guess v2.0.4
  Downloaded cfb v0.7.3
  Downloaded python-pkginfo v0.5.5
  Downloaded data-encoding v2.3.3
  Downloaded cargo-xwin v0.13.2
  Downloaded crypto-common v0.1.6
  Downloaded crunchy v0.2.2
  Downloaded cargo-options v0.5.3
  Downloaded pep440 v0.2.0
  Downloaded fat-macho v0.4.6
  Downloaded minijinja v0.27.0
  Downloaded lddtree v0.3.2
  Downloaded goblin v0.5.4
  Downloaded mailparse v0.13.8
  Downloaded rtoolbox v0.0.1
  Downloaded lock_api v0.4.9
  Downloaded goblin v0.6.0
  Downloaded smawk v0.3.1
  Downloaded tracing-subscriber v0.3.16
  Downloaded rayon v1.6.1
  Downloaded toml v0.5.10
  Downloaded tracing-core v0.1.30
  Downloaded sct v0.7.0
  Downloaded rpassword v7.2.0
  Downloaded webpki v0.22.0
  Downloaded clap_complete_command v0.4.0
  Downloaded bzip2-sys v0.1.11+1.0.8
  Downloaded linux-raw-sys v0.1.4
  Downloaded encoding_rs v0.8.31
  Downloaded ring v0.16.20
  Downloaded 155 crates (14.7 MB) in 4.27s (largest was `ring` at 5.1 MB)
   Compiling proc-macro2 v1.0.47
   Compiling quote v1.0.21
   Compiling libc v0.2.138
   Compiling unicode-ident v1.0.5
   Compiling syn v1.0.105
   Compiling version_check v0.9.4
   Compiling cfg-if v1.0.0
   Compiling memchr v2.5.0
   Compiling serde_derive v1.0.150
   Compiling serde v1.0.150
   Compiling once_cell v1.16.0
   Compiling autocfg v1.1.0
   Compiling log v0.4.17
   Compiling cc v1.0.77
   Compiling unicase v2.6.0
   Compiling proc-macro-error-attr v1.0.4
   Compiling getrandom v0.2.8
   Compiling proc-macro-error v1.0.4
   Compiling io-lifetimes v1.0.3
   Compiling bitflags v1.3.2
   Compiling heck v0.4.0
   Compiling os_str_bytes v6.4.1
   Compiling rustix v0.36.5
   Compiling itoa v1.0.4
   Compiling termcolor v1.1.3
   Compiling linux-raw-sys v0.1.4
   Compiling ring v0.16.20
   Compiling mime_guess v2.0.4
   Compiling lazy_static v1.4.0
   Compiling crossbeam-utils v0.8.14
   Compiling strsim v0.10.0
   Compiling ahash v0.7.6
   Compiling unicode-width v0.1.10
   Compiling serde_json v1.0.89
   Compiling regex-syntax v0.6.28
   Compiling typenum v1.16.0
   Compiling encoding_index_tests v0.1.4
   Compiling crc32fast v1.3.2
   Compiling crunchy v0.2.2
   Compiling byteorder v1.4.3
   Compiling is-terminal v0.4.1
   Compiling terminal_size v0.2.3
   Compiling clap_lex v0.3.0
   Compiling memoffset v0.7.1
   Compiling generic-array v0.14.6
   Compiling ryu v1.0.11
   Compiling spin v0.5.2
   Compiling adler v1.0.2
   Compiling tiny-keccak v2.0.2
   Compiling scopeguard v1.1.0
   Compiling minimal-lexical v0.2.1
   Compiling untrusted v0.7.1
   Compiling pkg-config v0.3.26
   Compiling proc-macro-hack v0.5.19
   Compiling either v1.8.0
   Compiling nom v7.1.1
   Compiling miniz_oxide v0.6.2
   Compiling bzip2-sys v0.1.11+1.0.8
   Compiling clap_derive v4.0.21
   Compiling aho-corasick v0.7.20
   Compiling crossbeam-epoch v0.9.13
   Compiling camino v1.1.1
   Compiling base64 v0.13.1
   Compiling time-core v0.1.0
   Compiling clap v4.0.29
   Compiling tinyvec_macros v0.1.0
   Compiling tinyvec v1.6.0
   Compiling regex v1.7.0
   Compiling time-macros v0.2.6
   Compiling rand_core v0.6.4
   Compiling scroll_derive v0.11.0
   Compiling flate2 v1.0.25
   Compiling hashbrown v0.12.3
   Compiling indexmap v1.9.2
   Compiling tracing-core v0.1.30
   Compiling anyhow v1.0.66
   Compiling thiserror v1.0.37
   Compiling fnv v1.0.7
   Compiling encoding_rs v0.8.31
   Compiling ppv-lite86 v0.2.17
   Compiling time v0.3.17
   Compiling rand_chacha v0.3.1
   Compiling const-random-macro v0.1.15
   Compiling unicode-linebreak v0.1.4
   Compiling toml v0.5.10
   Compiling scroll v0.11.0
   Compiling unicode-normalization v0.1.22
   Compiling thiserror-impl v1.0.37
   Compiling webpki v0.22.0
   Compiling terminal_size v0.1.17
   Compiling lock_api v0.4.9
   Compiling unicode-bidi v0.3.8
   Compiling percent-encoding v2.2.0
   Compiling rustls v0.20.7
   Compiling fastrand v1.8.0
   Compiling fs-err v2.9.0
   Compiling mime v0.3.16
   Compiling parking_lot_core v0.9.5
   Compiling plain v0.2.3
   Compiling rayon-core v1.10.1
   Compiling semver v1.0.14
   Compiling portable-atomic v0.3.16
   Compiling remove_dir_all v0.5.3
   Compiling smallvec v1.10.0
   Compiling tempfile v3.3.0
   Compiling form_urlencoded v1.1.0
   Compiling idna v0.3.0
   Compiling console v0.15.2
   Compiling crossbeam-deque v0.8.2
   Compiling const-random v0.1.15
   Compiling bzip2 v0.4.3
   Compiling rand v0.8.5
   Compiling crypto-common v0.1.6
   Compiling block-buffer v0.10.3
   Compiling tracing-attributes v0.1.23
   Compiling sct v0.7.0
   Compiling itertools v0.10.5
   Compiling regex-automata v0.1.10
   Compiling crossbeam-channel v0.5.6
   Compiling encoding-index-japanese v1.20141219.5
   Compiling encoding-index-korean v1.20141219.5
   Compiling encoding-index-singlebyte v1.20141219.5
   Compiling encoding-index-simpchinese v1.20141219.5
   Compiling encoding-index-tradchinese v1.20141219.5
   Compiling num_cpus v1.14.0
   Compiling thread_local v1.1.4
   Compiling bytes v1.3.0
   Compiling askama_escape v0.10.3
   Compiling overload v0.1.1
   Compiling uuid v1.2.2
   Compiling pin-project-lite v0.2.9
   Compiling cfb v0.7.3
   Compiling tracing v0.1.37
   Compiling nu-ansi-term v0.46.0
   Compiling askama_shared v0.12.2
   Compiling encoding v0.2.33
   Compiling matchers v0.1.0
   Compiling charset v0.1.3
   Compiling digest v0.10.6
   Compiling zip v0.6.3
   Compiling ahash v0.3.8
   Compiling url v2.3.1
   Compiling webpki-roots v0.22.6
   Compiling tracing-log v0.1.3
   Compiling tracing-serde v0.1.3
   Compiling socks v0.3.4
   Compiling sharded-slab v0.1.4
   Compiling dirs-sys v0.3.7
   Compiling chunked_transfer v1.4.0
   Compiling smawk v0.3.1
   Compiling cpufeatures v0.2.5
   Compiling lzxd v0.1.4
   Compiling number_prefix v0.4.0
   Compiling paste v1.0.10
   Compiling quoted_printable v0.4.6
   Compiling target-lexicon v0.12.5
   Compiling static_assertions v1.1.0
   Compiling twox-hash v1.6.3
   Compiling indicatif v0.17.2
   Compiling cab v0.4.1
   Compiling tracing-subscriber v0.3.16
   Compiling sha2 v0.10.6
   Compiling textwrap v0.16.0
   Compiling ureq v2.5.0
   Compiling dirs v4.0.0
   Compiling parking_lot v0.12.1
   Compiling chumsky v0.8.0
   Compiling askama_derive v0.11.2
   Compiling rayon v1.6.1
   Compiling msi v0.5.0
   Compiling versions v4.1.0
   Compiling goblin v0.6.0
   Compiling cargo-platform v0.1.2
   Compiling cargo-options v0.5.3
   Compiling clap_complete v4.0.6
   Compiling clap_derive v3.2.18
   Compiling which v4.3.0
   Compiling cli-table v0.4.7
   Compiling clap_lex v0.2.4
   Compiling xattr v0.2.3
   Compiling filetime v0.2.19
   Compiling atty v0.2.14
   Compiling bstr v0.2.17
   Compiling path-slash v0.2.1
   Compiling data-encoding v2.3.3
   Compiling same-file v1.0.6
   Compiling cbindgen v0.24.3
   Compiling walkdir v2.3.2
   Compiling mailparse v0.13.8
   Compiling globset v0.4.9
   Compiling clap v3.2.23
   Compiling tar v0.4.38
   Compiling xwin v0.2.10
   Compiling clap_complete_fig v4.0.2
   Compiling cargo_metadata v0.15.2
   Compiling askama v0.11.1
   Compiling rfc2047-decoder v0.2.0
   Compiling rustc_version v0.4.0
   Compiling combine v4.6.6
   Compiling goblin v0.5.4
   Compiling bincode v1.3.3
   Compiling uniffi_meta v0.21.0
   Compiling toml_datetime v0.5.0
   Compiling weedle2 v4.0.0
   Compiling rtoolbox v0.0.1
   Compiling glob v0.3.0
   Compiling lddtree v0.3.2
   Compiling uniffi_bindgen v0.21.0
   Compiling rpassword v7.2.0
   Compiling toml_edit v0.15.0
   Compiling cargo-zigbuild v0.14.2
   Compiling python-pkginfo v0.5.5
   Compiling cargo-xwin v0.13.2
   Compiling clap_complete_command v0.4.0
   Compiling ignore v0.4.18
   Compiling fat-macho v0.4.6
   Compiling multipart v0.18.0
   Compiling dialoguer v0.10.2
   Compiling pyproject-toml v0.3.1
   Compiling minijinja v0.27.0
   Compiling pep440 v0.2.0
   Compiling platform-info v1.0.1
   Compiling normpath v1.0.0
   Compiling configparser v3.0.2
   Compiling dunce v1.0.3
   Compiling bytesize v1.1.0
   Compiling maturin v0.14.6
    Finished release [optimized] target(s) in 5m 32s
  Installing /home/pokyuser/.cargo/bin/maturin
   Installed package `maturin v0.14.6` (executable `maturin`)
warning: be sure to add `/home/pokyuser/.cargo/bin` to your PATH to be able to run the installed binaries

------


bitbake cargo-bitbake-native

-----

cargo-bitbake-native -c addto_recipe_sysroot

----

oe-run-native cargo-bitbake-native cargo-bitbake --help

cargo-bitbake 0.3.16-alpha.0
Doug Goldstein <cardoe@cardoe.com>
Generates a BitBake recipe for a given Cargo project

USAGE:
    cargo <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    bitbake    Generates a BitBake recipe for a given Cargo project
    help       Prints this message or the help of the given subcommand(s)

----

oe-run-native cargo-bitbake-native cargo-bitbake bitbake --help

cargo-bitbake 0.3.16-alpha.0
Generates a BitBake recipe for a given Cargo project

USAGE:
    cargo bitbake [FLAGS]

FLAGS:
    -h, --help                Prints help information
    -l, --legacy-overrides    Legacy Overrides: Use legacy override syntax
    -q                        Silence all output
    -R                        Reproducible mode: Output exact git references for git projects
    -V, --version             Prints version information
    -v                        Verbose mode (-v, -vv, -vvv, etc.)

----

Let's try to creatre a bitbake recipe for maturin

cd /home/pokyuser/.cargo/registry/src/github.com-1ecc6299db9ec823/maturin-0.14.6

oe-run-native cargo-bitbake-native cargo-bitbake bitbake 
Running bitbake -e cargo-bitbake-native
Unable to determine git repo for this project
Wrote: maturin_0.14.6.bb

Slight hacks and one crate (zeroize) needed to be added.

bitbake maturin-native -c addto_recipe_sysroot

oe-run-native maturin-native maturin --help

Running bitbake -e maturin-native
Build and publish crates with pyo3, rust-cpython and cffi bindings as well as rust binaries as python packages

Usage: maturin <COMMAND>

Commands:
  build        Build the crate into python packages
  publish      Build and publish the crate as python packages to pypi
  list-python  Search and list the available python installations
  develop      Install the crate as module in the current virtualenv
  sdist        Build only a source distribution (sdist) without compiling
  init         Create a new cargo project in an existing directory
  new          Create a new cargo project
  upload       Upload python packages to pypi
  help         Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help information
  -V, --version  Print version information

-----


--- another attempt

bitbake cargo-native -c devshell

----

cd /workdir/build/raspberrypi4-64-homeassistant-master/workspace/sources/python3-orjson

bitbake cargo-bitbake-native -c addto_recipe_sysroot



!!!!!!!!!!!!!!!!!


pokyuser@s3-lxc-yp-bld-3:/workdir/build/raspberrypi4-64-homeassistant-master/tmp/work/cortexa72-resy-linux/python3-orjson/3.8.3-r0/orjson-3.8.3$ oe-run-native cargo-bitbake-native cargo-bitbake bitbake
Running bitbake -e cargo-bitbake-native
Unable to determine git repo for this project
Wrote: orjson_3.8.3.bb


